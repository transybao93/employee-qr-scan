import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import CORS from 'cors';
import config from './config.json';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import Connection from './db/connect';
import empRouter from './router/employee';
import partnerRouter from './router/partner';
import logtimeRouter from './router/logtime';
import dashboardRouter from './router/dashboard';
import statisticRouter from './router/statistic';
Connection.connect();
const app = express();

//- using morgan
app.use(morgan(':status [:method] :url - :response-time ms'))

//- body-parser
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

//- using compression
app.use(compression({
    level: 8
}))

//- using CORS for all request
app.use(CORS())

//- using helmet
app.use(helmet())

//- employee router
app.use('/emp', empRouter);

//- partner router
app.use('/partner', partnerRouter)

//- logtime router
app.use('/logtime', logtimeRouter)

//- dashboard router
app.use('/dashboard', dashboardRouter);

//- statistic router
app.use('/statistic', statisticRouter);

//- open a server
app.listen(process.env.PORT || config.port, () => {
    console.log('Server start at port: ' + (process.env.PORT || config.port));
})
