import _ from 'lodash';
import moment from 'moment';
import * as model from '../model/index';
import * as services from '../services/index';

class PartnerController 
{

    async create(req, res)
    {
        console.log('here at create new partner')
        try {
            let data = _.pick(req.body, ['pName', 'pEmail']);
            data.pPassword = req.encryptPassword;
            await model.Partner.create(data)
            .then(response => {
                //- return a token
                let tokenData = {
                    pCode: response.pCode
                }
                let token = services.token.generate(tokenData);
                // //- saved
                res.status(200).json({
                    error: false,
                    message: "Created!",
                    data: {
                        token: token
                    }
                })
            })
            .catch(err => {
                //- throw error
                console.error(err);
                res.status(500).json({
                    error: true,
                    data: err
                })
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showAllPartner(req, res){
        try {
            let partnerList = await model.Partner.find();
            res.status(200).json({
                error: false,
                data: partnerList
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showPartnerByCode(req, res){
        try {
            let partnerList = await model.Partner.find({
                pCode: req.params.code
            });
            res.status(200).json({
                error: false,
                data: partnerList
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }
    
    async showPartnerByID(req, res){
        try {
            let partner = await model.Partner.findById(req.params.id);
            res.status(200).json({
                error: false,
                data: partner
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showPartnerByEmail(req, res){
        try {
            let partner = await model.Partner.find({
                pEmail: req.params.email
            });
            res.status(200).json({
                error: false,
                data: partner
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async update(req, res){
        try {
            //- findone and update by id
            let data = _.pick(req.body, ['pName', 'pCode']);
            let partner = await model.Employee.findByIdAndUpdate(req.params.id, data, {
                new: true,
                runValidators: true
            })
            res.status(200).json({
                error: false,
                data: partner
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }
    async delete(req, res){
        try {
            await model.Partner.findByIdAndDelete(req.params.id);
            res.status().json({
                error: fasle,
                message: 'Deleted success!',
                data: null
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async createPartnerCode()
    {
        try {
            
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- show month statistic for partner
    async showMonthStatistic(req, res)
    {
        try {
            let partnerID = req.params.partnerID;
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }


}

export default new PartnerController();