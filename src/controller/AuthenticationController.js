import _ from 'lodash';
import moment from 'moment';
import {
    Employee,
    Partner
} from '../model/index';
import * as services from '../services/index'

class AuthenticationController 
{
    //- employee login
    async empLogin(req, res)
    {
        try {
            
            //- validate phone number and password
            let ePass = req.body.ePassword;
            let eEmail = req.body.eEmail;
            console.log('employee login email: %s - password %s', eEmail, ePass)

            //- get password by phone number
            let emp = await Employee.findOne({
                eEmail: eEmail,
            }).select(["+ePassword", "eEmail", "eName", "ePhoneNumber", "partner"]);

            //- compare password
            let isPasswordValid = services.bcryptjs.comparePass(ePass, emp.ePassword);

            if(isPasswordValid) //- if match
            {
                //- return with new token
                res.status(200).json({
                    message: 'Login success',
                    data: {
                        // eName: emp.eName,
                        // ePhoneNumer: emp.ePhoneNumber,
                        token: services.token.generate({
                            id: emp._id,
                            eName: emp.eName,
                            ePhoneNumer: emp.ePhoneNumber,
                            partner: emp.partner
                        })
                    },
                    role: 'employee'
                });

            }else{
                res.status(403).json({
                    message: 'Login fail',
                    data: null
                });
            }


        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- logout
    async empLogout(req, res)
    {
        try {
            //- expire the token

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- partner login
    async partnerLogin(req, res)
    {
        try {
            //- partner login
            //- validate phone number and password
            let partnerPass = req.body.pPassword;
            let email = req.body.pEmail;

            //- get password by phone number
            let partner = await Partner.findOne({
                pEmail: email
            }).select(["+pPassword", "pEmail", "pCode"]);

            console.log('authentication controller', partner)

            //- compare password
            let isPasswordValid = services.bcryptjs.comparePass(partnerPass, partner.pPassword);

            if(isPasswordValid) //- if match
            {
                //- return with new token
                res.status(200).json({
                    message: 'Login success',
                    data: {
                        pName: partner.pName,
                        pCode: partner.pCode,
                        token: services.token.generate({
                            eName: partner.eName,
                            pCode: partner.pCode,

                        })
                    },
                    role: 'partner'
                });

            }else{
                res.status(403).json({
                    message: 'Login fail',
                    data: null
                });
            }

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async partnerLogout()
    {
        try {
            //- partner logout

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- token check
    async tokenValidate(req, res)
    {
        try {
            

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- login using social account
    

}

export default new AuthenticationController();