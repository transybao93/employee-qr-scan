import EmployeeController from './EmployeeController';
import LogTimeController from './LogtimeController';
import PartnerController from './PartnerController';
import DashboardController from './DashboardController';
import AuthenticationController from './AuthenticationController';
import StatisticController from './StatisticController';

export {
    EmployeeController,
    LogTimeController,
    PartnerController,
    DashboardController,
    AuthenticationController,
    StatisticController
}