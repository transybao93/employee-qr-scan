import _ from 'lodash';
import moment from 'moment';
import * as model from '../model/index';

class LogtimeController 
{

    //- qr code check with empID
    //- check data before write in logtime
    async create(req, res){
        try {
            let data = _.pick(req.body, ['lTime', 'lDate', 'employee']);
            model.Employee.create(data)
            .then(response => {
                //- saved
                res.status(200).json({
                    error: false,
                    data: "Created!"
                })
            })
            .catch(err => {
                //- throw error
                console.error(err);
                res.status(500).json({
                    error: true,
                    data: err
                })
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showAllLogTime(req, res){
        try {
            let logTimeList = await model.Logtime.find();
            res.status(200).json({
                error: false,
                data: logTimeList
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showLogTimeByEmpID(req, res){
        try {
            let empID = req.body.empID;
            let logTime = await model.Logtime.findOne({
                employee: empID
            });
            res.status(200).json({
                error: false,
                data: logTime
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async updateLogTimeByEmpID(req, res){
        try {
            let data = _.pick(req.body, ['lTime', 'lDate']);
            let logTime = await model.Logtime.findOneAndUpdate({
                employee: req.body.empID
            },data, {
                new: true,
                runValidators: true
            })
            res.status(200).json({
                error: false,
                data: logTime
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }//- if needed

    async deleteLogTimeByEmpID(req, res){
        try {
            
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async QRScan(req, res)
    {
        try {
            //- save QRScan data
            //- lData will be current date
            let logtimeData = _.pick(req.body, 'lDate', 'lTime', 'lMonth');
            logtimeData.employee = req.id; //- employee id
            console.log('scan data', logtimeData)

            //- check dữ liệu của ngày hôm trước
            //- thống kê cho ngày hôm trước
            //- insert vào bảng statistic
            let ltc = new LogtimeController();
            //- check if current date is monday
            let currentDate = '';
            if(moment(req.body.lDate).format('dddd') === "Monday")
            {
                //- back to Saturday
                currentDate = moment(logtimeData.lDate).subtract(1, 'days');
            }else{
                currentDate = logtimeData.lDate;
            }
            await ltc.createStatistic(req.partnerID, req.id, currentDate);

            //- save to database
            await model.Logtime.create(logtimeData);
            res.status(200).json({
                error: false,
                message: "Created!"
            })

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }


    //- when user 
    //- POST
    //- show statistic about specific day
    //- contains late/ontime, late minutes
    async showDayStatistic(req, res)
    {   
        try {
            let empID = req.body.empID;
            let partnerID = req.body.partnerID;
            let date = req.body.date; //- 2018-11-23 => string
            let responseData = {};
            //- check if day < current day
            //- get first day and last day in the list
            if(moment(date).isAfter(moment()))
            {
                //- if selected day < current day
                let logtime = await model.Logtime.find({
                    lDate: date,
                    employee: empID
                })
                .sort({createdAt: 1})
                .select(['lDate', 'lTime', 'employee']);
                responseData.history = logtime;

                //- get in and out time
                let inTime = logtime[0].lTime;
                let outTime = _.last(logtime).lTime;
                //- get inLimit and outLimit from dashboard
                let initialInOutTime = await model.Dashboard.findOne({
                    partner: partnerID
                }).select(['inTimeLimit', 'outTimeLimit']);
                let defaultInTime = _.pick(initialInOutTime, 'inTimeLimit').inTimeLimit;
                let defaultOutTime = _.pick(initialInOutTime, 'outTimeLimit').outTimeLimit;
                
                
                //- compare go to work late
                if(moment(inTime).isAfter(defaultInTime))
                {
                    //- go to work late
                    responseData.isGoLate = true;
                }else{
                    //- ok
                    responseData.isGoLate = false;
                }

                //- compare leave work early
                if(moment(outTime).isBefore(defaultOutTime))
                {
                    //- go to work late
                    responseData.isLeaveEarly = true;
                }else{
                    //- ok
                    responseData.isLeaveEarly = false;
                }

            }
            
            res.status(200).json({
                error: false,
                message: "Fetch statistic success!",
                data: responseData
            })

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- when user 
    //- POST
    async showMonthStatistic(req, res)
    {   
        try {
            let empID = req.id;
            
            


        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- check current date and 
    //- create statistic for yesterday from current day
    //- for a single specific employee and partner
    async createStatistic(partnerID, empID, currentDate)
    {
        let data = {}
        //- if selected day < current day
        let yesterday = moment(currentDate).subtract(1, 'days').format('YYYY-MM-DD').toString();
        data.lDate = yesterday;
        data.employee = empID;

        console.log('yesterday', yesterday)

        let logtime = await model.Logtime.find({
            lDate: yesterday,
            employee: empID,
        })
        .sort({createdAt: 1})
        .select(['lDate', 'lTime', 'employee']);

        console.log('logtime size', _.size(logtime))

        let statistic = await model.Statistic.countDocuments({
            lDate: yesterday,
            employee: empID,
        });

        await console.log('count statistic', statistic)

        if(_.size(logtime) > 0 && statistic === 0)
        {
            console.log('yesterday statistic...')
            //- get in and out time
            let inTime = logtime[0].lTime;
            let outTime = _.last(logtime).lTime;
            //- get inLimit and outLimit from dashboard
            let initialInOutTime = await model.Dashboard.findOne({
                partner: partnerID
            }).select(['inTimeLimit', 'outTimeLimit']);
            let defaultInTime = _.pick(initialInOutTime, 'inTimeLimit').inTimeLimit;
            let defaultOutTime = _.pick(initialInOutTime, 'outTimeLimit').outTimeLimit;
            
            //- compare go to work late
            let d1 = moment(yesterday + 'T' + inTime + 'Z');
            let d2 = moment(yesterday + 'T' + defaultInTime + 'Z');
            console.log('d1 is after d2?', d1.isAfter(d2))
            if(d1.isAfter(d2))
            {
                //- go to work late
                data.isLate = true;
                data.lLateMinutes = d1.diff(d2, 'minute');
            }else{
                //- ok
                data.isLate = false;
                data.lLateMinutes = 0;
            }

            //- compare leave work early
            let d3 = moment(yesterday + 'T' + outTime + 'Z');
            let d4 = moment(yesterday + 'T' + defaultOutTime + 'Z');
            if(d3.isBefore(d4))
            {
                //- leave work early
                data.isEarly = true;
                data.lEarlyMinutes = d4.diff(d3, 'minute');
            }else{
                //- ok
                data.isEarly = false;
                data.lEarlyMinutes = 0;
            }

            //- insert in statistic
            data.lMonth = moment().format("MM");
            await model.Statistic.create(data);
            console.log('yesterday statistic....', data)
        }else{
            return;
        }
        

    }
    
    //- show history by employee id
    async showHistoryByEmpID(req, res)
    {
        try {
            let empID = req.params.empID;

            let logtime = await model.Logtime.find({
                employee: empID
            }).sort({createdAt: 1});
            let newLogtime = _.chain(logtime).groupBy('lDate');

            res.status(200).json({
                error: false,
                message: "Fetch history success",
                data: newLogtime
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }


}

export default new LogtimeController();