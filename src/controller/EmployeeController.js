import _ from 'lodash';
import moment from 'moment';
import * as model from '../model/index';
import * as services from '../services/index';

class EmployeeController 
{
    //- create new employee with each partner id
    //- update employee info
    async create(req, res){
        try {
            let data = _.pick(req.body, ['eName', 'eEmail', 'ePhoneNumber', 'eDeviceID', 'partner']);
            data.ePassword = req.encryptedRandomPass;
            console.log('random password', req.randomPass)
            console.log('encrypted random pass', req.encryptedRandomPass)
            await model.Employee.create(data)
            .then(async response => {

                //- send email
                //- apply queue
                await services.kue.emailQueue(data.eName, data.eEmail, req.randomPass);
                
                //- saved
                res.status(200).json({
                    error: false,
                    message: "Created!",
                    data: null
                })
            })
            .catch(err => {
                //- throw error
                console.error(err);
                res.status(500).json({
                    error: true,
                    data: err
                })
            });
           

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showAllEmp(req, res){
        try {
            let empList = await model.Employee.find();
            res.status(200).json({
                error: false,
                data: empList
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showEmpByID(req, res){
        try {
            let emp = await model.Partner.findById(req.params.id);
            res.status(200).json({
                error: false,
                data: emp
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showEmployeeByEmail(req, res){
        try {
            let emp = await model.Employee.find({
                eEmail: req.params.email
            });
            res.status(200).json({
                error: false,
                data: emp
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }
    
    //- update by id
    async update(req, res){
        try {
            //- findone and update by id
            let data = _.pick(req.body, ['eName', 'eEmail', 'ePhoneNumber', 'eDeviceID', 'ePassword']);
            data.ePassword = services.bcryptjs.genPass(data.ePassword);
            let emp = await model.Employee.findByIdAndUpdate(req.params.id, data, {
                new: true,
                runValidators: true
            })
            res.status(200).json({
                error: false,
                data: emp
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- update by token
    async updateByToken(req, res){
        try {
            //- findone and update by id
            let data = _.pick(req.body, ['eName', 'eEmail', 'ePhoneNumber', 'eDeviceID', 'ePassword', 'eOldPassword']);
            data.ePassword = services.bcryptjs.genPass(data.ePassword);
            let emp = await model.Employee.findOneAndUpdate({
                _id: req.id
            }, data, {
                new: true,
                runValidators: true
            })
            res.status(200).json({
                error: false,
                data: emp
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async delete(req, res){
        try {
            await model.Employee.findByIdAndDelete(req.params.id);
            res.status(200).json({
                error: false,
                message: 'Deleted success!',
                data: null
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async showEmpByPartnerID(req, res){
        try {
            
            let partnerID = req.params.partnerID;
            let updateData = req.body.data;
            let emp = await model.Employee.findOneAndUpdate({
                partner: partnerID
            }, updateData, {new: true, runValidators: true});
            res.status(200).json({
                error: false,
                message: "Success!",
                data: emp
            });

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- show month statistic for employee
    async showCurrentMonthStatistic(req, res)
    {
        try {
            let empID = req.params.empID;
            let currentMonth = req.params.currentMonth; //- number
            let logtime = await model.Logtime.find({
                lMonth: currentMonth,
                _id: empID
            }).sort({createdAt: 1});
            
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    async resetPass(req, res)
    {
        try {
            
            //- if email exist
            

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }


    //- just for testing
    async sendAnEmail(req, res)
    {
        try {
            
            await services.email.sendEmail('Bao', 'transybao28@gmail.com', '12354yhdgfsdftry');
            res.status(200).json({
                error: false,
                message: "Message sent !"
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- add email to jobs
    //- add job to queue
    //- start queue through queue process
    async sendEmailWithQueue(req, res)
    {
        try {
            
            await services.kue.toQueue('Bao', 'transybao28@gmail.com', '12354yhdgfsdftry');
            res.status(200).json({
                error: false,
                message: "Message sent !"
            });
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- in case not have email sent to email
    //- send a new random password again to existed account
    async resetSendEmail()
    {
        
    }
    
}

export default new EmployeeController();