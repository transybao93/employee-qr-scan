import _ from 'lodash';
import moment from 'moment';
import * as model from '../model/index';
import * as services from '../services/index'

class StatisticController 
{
    async create()
    {
        try {
            
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- show history by empID and by date range
    async showHistoryByWeek(req, res)
    {
        try {
            let chosenDate = req.params.chosenDate;
            // let chosenDate = '2018-12-24';
            console.log('chosen day', chosenDate)
            let empID = req.id;
            console.log('employee id', empID)
            let dateFrom = moment(chosenDate).startOf('isoWeek');
            let dateTo = moment(chosenDate).endOf('isoWeek');
            console.log('date from', dateFrom.format('YYYY-MM-DD'))
            console.log('date to', dateTo.format('YYYY-MM-DD'))

            let statistic = await model.Statistic.find({
                employee: empID,
                // createdAt: {
                //     $gte: new Date(moment(chosenDate).startOf('isoWeek')),
                //     $lte: new Date(moment(chosenDate).endOf('isoWeek'))
                // },
                lDate: {
                    $gte: dateFrom.format('YYYY-MM-DD'),
                    $lte: dateTo.format('YYYY-MM-DD')
                }
            }).sort({createdAt: 1});
            console.log('statistic list will response....', statistic)
            let newStatistic = _.chain(statistic).groupBy('lDate');

            res.status(200).json({
                error: false,
                message: "Fetch history success",
                data: newStatistic
            })
        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

    //- show month statistic by emp id
    async showMonthStatistic(req, res)
    {
        try{
            let empID = req.id;
            // let monthBefore = moment().subtract(1, 'months').format('MM');
            let monthBefore = 12;
            //- check if before month from current month has data or not
            // let monthStatistic = await model.Statistic.find({
            //     lMonth: monthBefore,
            //     employee: empID,
            // });
            let monthStatistic = await model.Statistic.aggregate([
                {
                    $match: {
                        $and: [
                            {
                                'lMonth': monthBefore,
                            },
                            {
                                'employee': empID, 
                            }
                        ]
                    }
                },
                // { 
                //     $group: {
                //         _id: null,
                //         totalLateMinutes: { $sum: "$lLateMinutes" },
                //         totalEarlyMinutes: { $sum: "$lEarlyMinutes" },
                //     }
                // }
            ]);
            console.log('month statistic', monthStatistic);
            res.status(200).json({
                error: false,
                message: "Success",
                data: monthStatistic
            })

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

}

export default new StatisticController();