import _ from 'lodash';
import moment from 'moment';
import * as model from '../model/index';

class DashboardController 
{
    //- compare time in or out
    async compare(req, res)
    {   
        try {
            
        } catch (error) {
            //- throw error
            console.error(err);
            res.status(500).json({
                error: true,
                data: err
            })
        }
    }

    async insertDefault(req, res)
    {
        try {
            let data = _.pick(req.body, ['inTimeLimit', 'outTimeLimit', 'partner']);
            await model.Dashboard.create(data);
            res.status(200).json({
                error: false,
                message: "Created!"
            });

        } catch (error) {
            //- throw error
            console.error(err);
            res.status(500).json({
                error: true,
                data: err
            })
        }
    }
}

export default new DashboardController();