

import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let StatisticSchema = new Schema({
    
    //- đi trễ / về sớm bao nhiêu phút
    lLateMinutes: {type: Number, required: true},
    lEarlyMinutes: {type: Number, required: true},

    //- đi trễ / về sớm vào ngày nào
    //- format: 2018-11-23
    //- type: String
    lDate: {type: String, required: true}, 
    lMonth: {type: Number, required: true},

    //- status
    isLate: {type: Boolean, required: true},//- checkin
    isEarly: {type: Boolean, required: true},//- checkout

    //- foreign key
    employee: [{ type: Schema.Types.ObjectId, ref: 'Employee' }]

})

const Statistic = mongoose.model('Statistic', StatisticSchema)
export default Statistic;
