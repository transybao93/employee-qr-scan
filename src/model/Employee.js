import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let EmployeeSchema = new Schema({
    
    eName: String,
    eEmail: {type: String, required: true, unique: true},
    // eCode: {type: String, required: true, unique: true},
    ePassword: {type: String, select: false, required: true},
    ePhoneNumber: {type: String},
    eDeviceID: String,

    //- foreign key
    partner: [{ type: Schema.Types.ObjectId, ref: 'Partner' }]

})

const Employee = mongoose.model('Employee', EmployeeSchema)
export default Employee;
