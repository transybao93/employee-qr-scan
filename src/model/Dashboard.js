import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let DashboardSchema = new Schema({

    inTimeLimit: String, //- go to work in what time (datetime format 2013-02-04T10:35:24Z)
    outTimeLimit: String, //- go home in what time (datetime format 2013-02-04T10:35:24Z)

    //- one partner have many dashboard
    partner: [{ type: Schema.Types.ObjectId, ref: 'Partner' }]

})

const Dashboard = mongoose.model('Dashboard', DashboardSchema)
export default Dashboard;
