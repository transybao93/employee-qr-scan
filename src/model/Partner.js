import mongoose from 'mongoose';
import moment from 'moment';
//- plugin
import {
    defaultRandomNumber
} from '../mongoose-plugin/index';

let Schema = mongoose.Schema;
let PartnerSchema = new Schema({
    
    pName: String,
    pEmail: {type: String, unique: true},
    pPassword: {type: String, select: false},
    pCode: {type: String, unique: true}, //- must be unique, and a random string

})

PartnerSchema.plugin(defaultRandomNumber);

const Partner = mongoose.model('Partner', PartnerSchema)
export default Partner;
