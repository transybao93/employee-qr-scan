import Employee from './Employee';
import Partner from './Partner';
import Logtime from './Logtime';
import Dashboard from './Dashboard';
import Statistic from './Statistic';

export {
    Employee,
    Partner,
    Logtime,
    Dashboard,
    Statistic
}