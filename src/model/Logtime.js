import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let LogtimeSchema = new Schema({
    //- string 16:00:00
    lTime: {type: String, required: true}, 
    //- string 2018-11-23
    lDate: {type: String, required: true}, 

    //- 2018-11-23T16:00:00Z => will be a string
    // lDateTime: {type: Date, required: true},
    lMonth: {type:Number, required: true},

    //- foreign key
    employee: [{ type: Schema.Types.ObjectId, ref: 'Employee' }]

})

const Logtime = mongoose.model('Logtime', LogtimeSchema)
export default Logtime;
