import moment from 'moment';
import _ from 'lodash';
import * as services from '../services';
import * as model from '../model';

class MUser
{
    async checkPhoneNumber(req, res, next)
    {
        let phoneNumber = req.body.phoneNumber;
        try {
            let count = await model.Employee.estimatedDocumentCount({
                uPhoneNumber: phoneNumber
            });
            console.log('count', count);
            if(count === 0) //- exist
            {   
                //- phone number not exist
                //- verify phone number then redirect to new password page
                sendCode();
                //- check timezone
                res.status(200).json({
                    message: "New phone number",
                    isNewPhone: true,
                    data: {
                        newPhoneNumber: phoneNumber
                    },
                    expireDateTime: moment(new Date()).add(30, 'm').utcOffset("+0700").format("D/MM/YYYY HH:mm:ss")
                });
            }else{
                //- phone number already exist
                next();
            }
        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
        
    }

    async checkPhoneNumberExist(req, res, next)
    {
        try {
            
            let count = await model.Employee.countDocuments({
                uPhoneNumber: req.body.uPhoneNumber
            });
            if(count !== 0)
            {
                next();
            }else{
                res.status(404).json({
                    message: 'Phone number is not found',
                    data: null
                })
            }

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async encryptPassword(req, res, next)
    {
        try {
            //- call token service
            req.encryptPassword = await services.bcryptjs.genPass(req.body.ePassword);
            next();

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async generateRandomPassword(req, res, next)
    {
        try {
            //- call token service
            let randomPass = await services.ultilities.randomPass();
            req.randomPass = randomPass;
            req.encryptedRandomPass = await services.bcryptjs.genPass(randomPass);
            next();

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async verifyToken(req, res, next) {
        try {
            //- verify token
            let tokenValid = services.token.verify(req.body.token);

            if(tokenValid !== false)
            {
                console.log('token infor', tokenValid)
                res.status(200).json({
                    message: "Token valid...",
                    data: tokenValid
                });
            }else{
                res.status(200).json({
                    message: "Token is not valid...",
                    data: null
                });
            }

            
        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async verifyTokenFromHeader(req, res, next) {
        try {
            console.log('here at verify token middleware')
            //- verify token
            let token = req.headers.authorization.split(" ")[1];
            console.log('token from header', token)
            let tokenValid = await services.token.verify(token);

            if(tokenValid !== false)
            {
                req.id = tokenValid.id;
                req.partnerID = tokenValid.partner.toString();
                console.log('partner id from header', req.partnerID)
                console.log('token id from header', tokenValid.id)
                next();
            }else{
                res.status(200).json({
                    message: "Token is not valid...",
                    data: null
                });
            }

            
        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async verifyUserPhoneNumber(req, res, next)
    {
        try {
            //- check user by phone number and uVerified
            let user = await model.Employee.findOne({
                ePhoneNumber: req.phoneNumber,
            });
            req.userID = user._id;
            if(req.userID !== null)
            {
                next();
            }else{
                res.status(500).json({
                    message: "User not found or the phone number is not verified",
                    data: null
                });
            }
            
        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    //- check password valid with id
    async checkPasswordValid(req, res, next)
    {
        console.log('here at check password valid middleware')
        if(_.size(req.body.ePassword) > 0 && _.size(req.body.eOldPassword) > 0) //- check if update password
        {
            //- check old password
            let emp = await model.Employee.findById(req.id).select(["+ePassword"]);
            let isPasswordValid = await services.bcryptjs.comparePass(req.body.eOldPassword, emp.ePassword);
            if(isPasswordValid)
            {
                next();
            }else{
                res.status(403).json({
                    error: true,
                    message: "Your old password is not valid. Please try again",
                    data: null
                })
            }
            
        }
    }

    async checkEmailExist(req, res, next)
    {
        try {
            let email = req.body.email;
            //- check if email exist, then next()
            let count = model.Employee.countDocuments({
                eEmail: email
            });

            if(count > 0)
            {
                next();
            }else{
                res.status(404).json({
                    error: true,
                    message: "This email is not exist",
                    data: null
                });
            }

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

}

export default new MUser();
