import moment from 'moment';
import _ from 'lodash';
import * as services from '../services/index';
import * as model from '../model/index';

class MPartner
{
    
    async encryptPassword(req, res, next)
    {
        try {
            //- call token service
            req.encryptPassword = await services.bcryptjs.genPass(req.body.pPassword);
            next();

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

}

export default new MPartner();
