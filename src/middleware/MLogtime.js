import moment from 'moment';
import _ from 'lodash';
import * as services from '../services/index';
import * as model from '../model/index';

class MLogtime
{
    
    async checkPartnerEmployee(req, res, next)
    {
        try {
            
            //- check if partner id and employee valid 
            let partnerID = req.partnerID;
            let count = await model.Employee.countDocuments({
                partner: partnerID,
                _id: req.id //- employee id
            });         
            if(count > 0)
            {
                //- exist
                next();
            }else{
                res.status(200).json({
                    error: true,
                    message: "Invalid employee with this partner. Please check again!"
                })
            }

        } catch (error) {
            //- throw error
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }
    }

}

export default new MLogtime();
