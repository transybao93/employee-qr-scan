import express from 'express';
import MLogtime from '../middleware/MLogtime';
import MEmp from '../middleware/MEmp';
import * as controller from '../controller';
const router = express.Router();

//- scan qr code
router.post('/scan', [
    MEmp.verifyTokenFromHeader,
    MLogtime.checkPartnerEmployee,
], controller.LogTimeController.QRScan);

//- show history
router.get('/history/:empID', controller.LogTimeController.showHistoryByEmpID);

export default router;