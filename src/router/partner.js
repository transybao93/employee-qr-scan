import express from 'express';
import MEmp from '../middleware/MEmp';
import MPartner from '../middleware/MPartner';
import * as controller from '../controller';
const router = express.Router();

//- register
router.post('/register', [
    MPartner.encryptPassword, 
], controller.PartnerController.create); //- a middleware to generate token and encrypt password

//- router
router.post('/token/check', [ MEmp.verifyToken ]);

//- login
router.post('/login', [ 
    MEmp.verifyTokenFromHeader
], controller.AuthenticationController.partnerLogin); //- if phone number and password valid => redirect to homepage

//- show all partner
router.get('/all', controller.PartnerController.showAllPartner);

//- show partner by id
router.get('/:id', controller.PartnerController.showPartnerByID);

//- show partner by partner code
router.get('/:code', controller.PartnerController.showPartnerByCode);

//- show partner by email
router.get('/:email', controller.PartnerController.showPartnerByEmail);

//- update partner information by partner id
router.patch('/update/:id', controller.PartnerController.update);

//- delete partner by id
router.delete('/delete/:id', controller.PartnerController.delete);


export default router;