import express from 'express';
import MLogtime from '../middleware/MLogtime';
import MEmp from '../middleware/MEmp';
import * as controller from '../controller';
const router = express.Router();

//- show history by week determine from chosenDate
router.get('/history/by/week/:chosenDate', [
    MEmp.verifyTokenFromHeader
], controller.StatisticController.showHistoryByWeek);

//- show month statistic
router.get('/history/by/month', [
    MEmp.verifyTokenFromHeader
], controller.StatisticController.showMonthStatistic);
export default router;