import express from 'express';
import MEmp from '../middleware/MEmp';
import * as controller from '../controller';
const router = express.Router();

router.post('/create', controller.DashboardController.insertDefault);

//- 

export default router;
