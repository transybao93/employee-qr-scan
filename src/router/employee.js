import express from 'express';
import MEmp from '../middleware/MEmp';
import MAuthentication from '../middleware/MAuthentication';
import * as controller from '../controller';
const router = express.Router();

//- register
router.post('/register', [
    MEmp.generateRandomPassword
], controller.EmployeeController.create); //- a middleware to generate token and encrypt password

//- router
router.post('/token/check', [ MEmp.verifyToken ]);//- if token valid => redirect to homepage

//- login
router.post('/login', [
    MAuthentication.checkEmailExist
], controller.AuthenticationController.empLogin); //- if phone number and password valid => redirect to homepage

//- show all employee
router.get('/all', controller.EmployeeController.showAllEmp);

//- show all employee by id
router.get('/:id', controller.EmployeeController.showEmpByID);

//- show employee by partner id
router.get('/:partnerID', controller.EmployeeController.showEmpByPartnerID);

//- show employee by email
router.get('/:email', controller.EmployeeController.showEmployeeByEmail);

//- update employee information by id
router.patch('/update/:id', controller.EmployeeController.update);

//- update employee information by token
router.patch('/update/by/token', [
    MEmp.verifyTokenFromHeader,
    MEmp.checkPasswordValid
], controller.EmployeeController.updateByToken);

//- delete employee by id
router.delete('/delete/:id', controller.EmployeeController.delete);

//- send an email to test
router.post('/send/test/email', controller.EmployeeController.sendAnEmail);

//- send an email with queue
router.post('/send/email/with/queue', controller.EmployeeController.sendEmailWithQueue);

export default router;
