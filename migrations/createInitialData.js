import faker from 'faker';
import mongoose from 'mongoose';
import * as model from '../src/model/index';
import {genPass} from '../src/services/bcryptjs';
const _ = require('lodash');
import Connection from '../src/db/connect';
Connection.connect();

//- create new partner
for(let i=0; i<5; i++) {
    let partner = {};
    partner.pName = faker.name.findName();
    partner.pEmail = faker.internet.email();
    partner.pPassword = genPass("123456");
    model.Partner.create(partner, function(error, partner) {
        if(error) throw error;
        //- create a new user data
        let employee = {};
        employee.eName = faker.name.findName();
        employee.eEmail = faker.internet.email();
        employee.ePhoneNumber = faker.phone.phoneNumber();
        employee.eDeviceID = "DFGHJ3456789GBN";
        employee.partner = partner._id;
        employee.ePassword = genPass("123456");
        model.Employee.create(employee, function(error, emp) {
            if(error) throw error;
            if(_.size(emp) > 0)
            {
                console.log("Generate initial data is completed...");
                mongoose.connection.close();
            }else{
                throw 'Invalid employee creation...';
            }
        });

  });
} 